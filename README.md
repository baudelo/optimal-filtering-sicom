# Filtrage Linéaire Optimal
## `News`

<!--


##### Suivi pédagogique
Voici le lien **zoom** pour suivre les séances en visioconférence si vous êtes tenu à l'isolement pour des *raisons sanitaires*  (ce lien est également indiqué sur la  [page chamilo](https://chamilo.grenoble-inp.fr/courses/PHELMA4PMSFIL9/index.php?) du cours) :
- https://grenoble-inp.zoom.us/j/99165253858
- *Passcode:* 281492Z

Dans ce cas, *merci de me prévenir également par mail* [florent.chatelain@grenoble-inp.fr](mailto:Florent.Chatelain@grenoble-inp.fr) *avant la séance.*
-->

<!--
#### Travail à préparer pour le Vendredi 26 février

- Relire le début le la leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/5_Kalman.pdf) sur le filtrage de Kalman jusqu'à l'exemple du suivi d'une fusée <u>slide 10</u>.
- BE : lire et exécuter le notebook
[N8_kalman_suivi_fusee.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/5_Kalman/N8_kalman_suivi_fusee.ipynb)  du suivi de trajectoire d'une fusée au décollage. Voir l'effet des paramètres du modèle comme les variances des bruits de modèle/mesure sur le résultat de l'estimation (cf. questions à la fin du notebook)

#### Programme séance 07 : Vendredi 26 février (8h15-10h15, distanciel)
- Fin de la leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/5_Kalman.pdf)
sur le filtre de Kalman.
- BE : illustration du filtrage de Kalman pour le suivi de trajectoire d'une fusée au décollage,   notebook
[N8_kalman_suivi_fusee.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/5_Kalman/N8_kalman_suivi_fusee.ipynb).

#### ~~Travail à préparer pour le Lundi  22 février~~

- ~~Relire la leçon [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/4_filtrage_adaptatif.pdf)  sur les algorithmes adaptatifs jusqu'au <u>slide 17</u>.~~
- ~~TD : montrer que l'estimateur de la moyenne empirique obtenu avec les $`n`$ observations $`y[k]`$ où $`k= 1,...,n`$ (i.e. $`\hat{m}_n = \frac1n \sum_{k=1}^{n} y[k]`$) peut s'écrire sous une forme récursive de type prédiction-correction, i.e. $`\hat{m}_n = \hat{m}_{n-1} + \gamma_n (y[n]- \hat{m}_{n-1})`$ et interpréter l'effet du gain lorsque $`n`$ tends vers l'infini~~

#### ~~Programme séance 06 : Lundi 22 février (10h30-12h30, distanciel)~~
- ~~Fin de la leçon [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/4_filtrage_adaptatif.pdf)  : filtrage adaptatif (LMS et RLS)~~
- ~~TD : notion d'adaptativité~~
- ~~BE : illustrations du filtrage adaptatif sur différents problèmes/notebooks~~
  - ~~[N6_annulation_echo.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N6_annulation_echo.ipynb) sur l'annulation d'écho pour la *téléphonie main libre*~~
  - ~~[N7_ecg_foetal-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N7_ecg_foetal-template.ipynb) sur l'expérience de Widrow afin d'estimer l'ECG du foetus chez la femme enceinte~~
-  ~~Début de la  leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/5_Kalman.pdf) sur le filtre de Kalman~~

#### ~~Travail à préparer pour le Vendredi 12 février~~

  - ~~Relire la leçon [3_Wiener_discret-modeles_AR.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/3_Wiener_discret-modeles_AR.pdf) sur le filtre de Wiener discret à mémoire finie et les modèles AR jusqu'au <u>slide 15</u>.~~
  - ~~Regarder/exécuter le notebook pour illustrer la mod&eacute;lisation de signaux de parole
  [N4_analyse_voyelle.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~

#### ~~Séance 05 : Vendredi 12 février (8h15-10h15, distanciel)~~
- ~~BE : analyse de parole <u>slide 17 &agrave; 24</u>&nbsp; pour illustrer la mod&eacute;lisation de signaux de parole (vocodeur), notebook
[N4_analyse_voyelle.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~
  - ~~Analyser diff&eacute;rents fichiers audio (plusieurs voyelles)~~
  - ~~Voir l&#39;effet &quot;vois&eacute;/non vois&eacute;&quot; et la &quot;hauteur/valeur&quot; du pitch sur la synth&egrave;se de son apr&egrave;s estimation des coefficients AR.~~
- ~~slides [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/4_filtrage_adaptatif.pdf)  : filtrage adaptatif (LMS et RLS)~~




#### ~~Séance 4 :  Lundi 8 février (10h30-12h30, distanciel)~~
- ~~slides [3_Wiener_discret-modeles_AR.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/3_Wiener_discret-modeles_AR.pdf) sur le filtre de Wiener discret et les modèles AR.~~
- ~~BE : filtre de Wiener auto-ajusté pour débruiter les signaux EMG, notebook [N3_denoising_EMG-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N3_denoising_EMG-template.ipynb)~~


#### ~~Travail à préparer pour le Vendredi 5 février~~
~~Relire la fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) sur le filtre de Wiener causal discret.  Bien comprendre la condition de causalité en TZ (slide 27).~~

#### ~~Séance 3 : Mercredi 27 janvier (8h15-10h15, distanciel)~~

- ~~fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) sur la synthèse du filtre causal.~~
- ~~TD : exercice d'application du filtre de Wiener causal au débruitage:~~

  <img alt="\left\{ \begin{array}{rcl} Y(t) &amp; = &amp; X(t) + V(t)\\ S(t) &amp; = &amp; X(t) \end{array} \right." height="45" src="https://latex.codecogs.com/gif.latex?%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Brcl%7D%20Y%28t%29%20%26%20%3D%20%26%20X%28t%29%20&amp;plus;%20V%28t%29%5C%5C%20S%28t%29%20%26%20%3D%20%26%20X%28t%29%20%5Cend%7Barray%7D%20%5Cright."/>,

  ~~avec~~

  <img alt="\left\{ \begin{array}{rcl} \gamma_{XX}(\nu) &amp; = &amp; \frac{5}{(\nu^2+4)^2}\\ &amp; &amp;\\ \gamma_{VV}(\nu) &amp; = &amp; \frac{1}{(\nu^2+4)} \end{array} \right." src="https://latex.codecogs.com/gif.latex?%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Brcl%7D%20%5Cgamma_%7BXX%7D%28%5Cnu%29%20%26%20%3D%20%26%20%5Cfrac%7B5%7D%7B%28%5Cnu%5E2&amp;plus;4%29%5E2%7D%5C%5C%20%26%20%26%5C%5C%20%5Cgamma_%7BVV%7D%28%5Cnu%29%20%26%20%3D%20%26%20%5Cfrac%7B1%7D%7B%28%5Cnu%5E2&amp;plus;4%29%7D%20%5Cend%7Barray%7D%20%5Cright." />

 - ~~Trouver la réponse en fréquence et proposer un schéma de principe pour le filtrage (analogique) : montage série/parallèle pour obtenir l'estimée de S à partir de Y.~~
 - ~~Version discrète du filtre de Wiener causal et anti-causal (fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf).~~


#### ~~Travail à préparer pour le Mercredi 27 janvier (8h15-10h15, distanciel)~~

 - ~~relire les slides [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) vus ensemble (filtre de Wiener **causal** et approche de Bode-Shannon) jusqu'au *slide 12*~~
 - ~~continuer la lecture de l'approche de Bode-Shannon jusqu'au slide *slide 18*~~
 - ~~TD : exercice *slide 19* afin d'illustrer le théorème de Paley Wiener (regarder dans le domaine spectral les poles des contributions causale et anticausale)~~


#### ~~Séance 2 : Mardi 19 janvier (8h15-10h15, **distanciel**)~~

 - ~~BE: applications en filtrage inverse~~
   - ~~notebook [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~
   - ~~notebook [N2_deconvolution_image_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/1_Wiener_non_causal/N2_deconvolution_image_Wiener_smoothing.ipynb)~~
 - ~~slides [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) : filtre de Wiener **causal** et approche de Bode-Shannon, jusqu'au *slide 12*~~

#### ~~Travail à préparer pour le Mardi 19 janvier (8h15-10h15, distanciel)~~

 - ~~relire les slides [1_Wiener_non_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) jusqu'au *slide 22*~~
 - ~~terminer l'exercice : interpréter les résultats dans les cas limites (grand ou petit rapport signal à bruit fréquentiel)~~
 - ~~regarder le notebook [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/1_Wiener_non_causal/)~~
   - ~~lire et *exécuter* ce notebook (et &eacute;couter les signaux consid&eacute;r&eacute;s)~~
   - ~~interpr&eacute;ter la forme du filtre optimal. Que se passe t&#39;il si l&#39;on augmente ou diminue le rapport signal &agrave; bruit (SNR) ?~~
   - ~~Quelle hypoth&egrave;se fondamentale requise en th&eacute;orie pour le filtre de Wiener n&#39;est pas respect&eacute;e dans cet exemple ?~~

#### ~~Séance 1 : Mardi 12 janvier (8h15-10h15, **distanciel**)~~

~~Le programme sera le suivant~~
 - ~~slides [1_Wiener_non_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) : introduction + filtre de Wiener non causal + exemples/applications~~
 - ~~TD : exercice slide 22~~
-->

<!--
#### Travail pour le Vendredi 28 janvier (10h30-12h30)
- Lire la fin des [slides](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/1_Wiener_non_causal.pdf) sur les exemples de filtrage inverse
 (pp. 23-29) et exécuter le notebook associé
[N1_denoising_chirp_Wiener_smoothing.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb), et répondre aux questions suivantes *de manière synthétique* :
  1. Le débruitage obtenu vous semble t'il satisfaisant ?
  2. Comment interpréter le gain (fonction de tranfert) du filtre de Wiener non causal tracé à la cellule [19] ?
  3. Quel serait ici l'avantage du filtre de Wiener par rapport à un simple filtre passe bande ?
  4. Quelle hypothèse fondamentale requise par le filtre de Wiener n'est ici pas respectée ? Que conclure ?
- **Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via
cette [tâche Chamilo](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA4PMSFIL9&id_session=0&gidReq=0&gradebook=0&origin=&id=221412&isStudentView=true)
-->

<!--
#### Séance 06 : Mardi 15 février
- Fin de la leçon [4_filtrage_adaptatif.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/4_filtrage_adaptatif.pdf)  : filtrage adaptatif LMS
- BE : illustrations du filtrage adaptatif sur différents problèmes/notebooks
  - [N6_annulation_echo.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/4_filtrage_adaptatif/N6_annulation_echo.ipynb) sur l'annulation d'écho pour la *téléphonie main libre*
  - [N7_ecg_foetal-template.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/4_filtrage_adaptatif/N7_ecg_foetal-template.ipynb) sur l'expérience de Widrow afin d'estimer l'ECG du foetus chez la femme enceinte
-  Début de la  leçon [5_Kalman.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/5_Kalman.pdf) sur le filtre de Kalman



#### ~~Séance 05 : Vendredi 11 février~~

- ~~BE : analyse de parole <u>slide 17 &agrave; 24</u>&nbsp; pour illustrer la mod&eacute;lisation de signaux de parole (vocodeur), notebooks~~
  - ~~[N4_analyse_voyelle.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~
  - ~~[N5_vocoder_lpc.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N5_vocoder_lpc.ipynb)~~
- ~~TD : notion d'adaptativité (exemple de l'estimateur récursif de la moyenne)~~
- ~~slides [4_filtrage_adaptatif.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/4_filtrage_adaptatif.pdf)  : filtrage adaptatif RLS~~


#### ~~Travail à préparer pour le Vendredi 11 février~~

- ~~Lire les [<u>slide 17 &agrave; 20</u>&nbsp;](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/3_Wiener_discret-modeles_AR.pdf) pour illustrer la mod&eacute;lisation  AR
sur des signaux de son voisé,~~
- ~~BE : exécuter le notebook associé
[N4_analyse_voyelle.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~
  - ~~jouer les sons enregistrés et synthétisés par modèle AR,  et interpréter les résultats obtenus (signal d'excitation, information spectrale, fidelité du signal synthétique) et~~
  - ~~voir l&#39;effet de la &quot;hauteur&quot;, i.e. la valeur du pitch, sur la synth&egrave;se de son apr&egrave;s estimation des coefficients AR.~~

#### ~~Séance 4 : Mardi  février (10h30-12h30)~~
- ~~slides [3_Wiener_discret-modeles_AR.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/3_Wiener_discret-modeles_AR.pdf) sur le filtre de Wiener discret et les modèles AR.~~
- ~~BE : application du filtre de Wiener auto-ajusté pour débruiter les signaux EMG, notebook [N3_denoising_EMG-template.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N3_denoising_EMG-template.ipynb)~~


#### ~~Séance 3 : Vendredi 4 février (10h30-12h30)~~

- ~~fin de la leçon [2_Wiener_causal.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/2_Wiener_causal.pdf) sur la synthèse du filtre causal.~~
- ~~TD : exercice d'application du filtre de Wiener causal au débruitage:~~

  <img alt="\left\{ \begin{array}{rcl} Y(t) &amp; = &amp; X(t) + V(t)\\ S(t) &amp; = &amp; X(t) \end{array} \right." height="45" src="https://latex.codecogs.com/gif.latex?%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Brcl%7D%20Y%28t%29%20%26%20%3D%20%26%20X%28t%29%20&amp;plus;%20V%28t%29%5C%5C%20S%28t%29%20%26%20%3D%20%26%20X%28t%29%20%5Cend%7Barray%7D%20%5Cright."/>,

  ~~avec~~
  <img alt="\left\{ \begin{array}{rcl} \gamma_{XX}(\nu) &amp; = &amp; \frac{5}{(\nu^2+4)^2}\\ &amp; &amp;\\ \gamma_{VV}(\nu) &amp; = &amp; \frac{1}{(\nu^2+4)} \end{array} \right." src="https://latex.codecogs.com/gif.latex?%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Brcl%7D%20%5Cgamma_%7BXX%7D%28%5Cnu%29%20%26%20%3D%20%26%20%5Cfrac%7B5%7D%7B%28%5Cnu%5E2&amp;plus;4%29%5E2%7D%5C%5C%20%26%20%26%5C%5C%20%5Cgamma_%7BVV%7D%28%5Cnu%29%20%26%20%3D%20%26%20%5Cfrac%7B1%7D%7B%28%5Cnu%5E2&amp;plus;4%29%7D%20%5Cend%7Barray%7D%20%5Cright." />

 - ~~Trouver la réponse en fréquence et proposer un schéma de principe pour le filtrage (analogique) : montage série/parallèle pour obtenir l'estimée de S à partir de Y.~~
 -~~Version discrète du filtre de Wiener causal et anti-causal (fin de la leçon [2_Wiener_causal.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/2_Wiener_causal.pdf)).~~


####  ~~Séance 2 : Vendredi 28 janvier (10h30-12h30)~~
 - ~~fin exercice de débruitage détaillé sur les [slides](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/1_Wiener_non_causal.pdf) p.22~~
 - ~~BE: applications en filtrage inverse~~
   - ~~notebook [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~
   - ~~notebook [N2_deconvolution_image_Wiener_smoothing.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/1_Wiener_non_causal/N2_deconvolution_image_Wiener_smoothing.ipynb)~~
 - ~~slides [2_Wiener_causal.pdf](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/2_Wiener_causal.pdf) : filtre de Wiener **causal** et approche de Bode-Shannon, jusqu'au *slide 12*~~

#### ~~Travail pour le Vendredi 28 janvier (10h30-12h30)~~
- ~~Terminer l'exercice de débruitage détaillé sur les [slides](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/1_Wiener_non_causal.pdf) p.22 : Donner l'expression de gain complexe $`G_{\textrm{NC}}(\nu)`$ du filtre optimal et montrer que la densité spectrale de l'erreur a pour expression
$`\gamma_{EE}(\nu) = \tfrac{\gamma_{XX}(\nu)\gamma_{VV}(\nu) }{\gamma_{XX}(\nu) + \gamma_{VV}(\nu)}`$
(*Indice : la TF d'une fonction réelle et paire est une fonction réelle et paire*). **Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via
cette [tâche Chamilo](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA4PMSFIL9&id_session=0&gidReq=0&gradebook=0&origin=&id=221412&isStudentView=true).~~
- ~~Lire la fin des [slides](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/slides/1_Wiener_non_causal.pdf) sur les exemples de filtrage inverse
 (pp. 23-29) et exécuter le notebook associé [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gitlab.ensimag.fr/ds-courses/2a-sicom-filtrage-optimal/-/blob/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~

-->

#### Séance 7 : Jeudi 2 février
- Fin de la leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/5_Kalman.pdf)
sur le filtre de Kalman.
- BE : illustration du filtrage de Kalman pour le suivi de trajectoire d'une fusée au décollage,   notebook
[N8_kalman_suivi_fusee.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/5_Kalman/N8_kalman_suivi_fusee.ipynb).


#### ~~Séance 6 : Mardi 31 janvier~~
- ~~Fin de la leçon [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/4_filtrage_adaptatif.pdf)  : filtrage adaptatif LMS~~
- ~~BE : illustrations du filtrage adaptatif sur différents problèmes/notebooks~~
  - ~~[N6_annulation_echo.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N6_annulation_echo.ipynb) sur l'annulation d'écho pour la *téléphonie main libre*~~
  - ~~[N7_ecg_foetal-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/4_filtrage_adaptatif/N7_ecg_foetal-template.ipynb) sur l'expérience de Widrow afin d'estimer l'ECG du foetus chez la femme enceinte~~
-  ~~Début de la  leçon [5_Kalman.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/5_Kalman.pdf) sur le filtre de Kalman~~

#### ~~Séance 5 : Vendredi 27 janvier (10h30-12h30)~~

- ~~fin du BE : analyse de parole <u>slide 17 &agrave; 24</u> pour illustrer la modélisation de signaux de parole (vocodeur), notebook~~
  - ~~[N5_vocoder_lpc.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N5_vocoder_lpc.ipynb)~~
- ~~TD : notion d'adaptativité (exemple de l'estimateur récursif de la moyenne)~~
- ~~slides [4_filtrage_adaptatif.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/slides/4_filtrage_adaptatif.pdf) : filtrage adaptatif RLS~~

#### ~~Séance 4 : Vendredi 20 janvier (10h30-12h30)~~

- ~~slides [3_Wiener_discret-modeles_AR.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/3_Wiener_discret-modeles_AR.pdf) sur le filtre de Wiener discret et les modèles AR.~~
- ~~BE : application du filtre de Wiener auto-ajusté pour débruiter les signaux EMG, notebook [N3_denoising_EMG-template.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N3_denoising_EMG-template.ipynb)~~
- ~~BE : analyse de parole pour illustrer la modélisation de sons voisés, notebook~~
  - ~~[N4_analyse_voyelle.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/blob/master/notebooks/3_Wiener_discret-modeles_AR/N4_analyse_voyelle.ipynb)~~


#### ~~Séance 3 : Mercredi 18 janvier (10h30-12h30)~~

- ~~fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) sur la synthèse du filtre causal.~~
- ~~TD : exercice d'application du filtre de Wiener causal au débruitage:~~

  $`\left\{ \begin{array}{rcl} Y(t) & = & X(t) + V(t)\\ S(t) & = & X(t) \end{array} \right.`$

  ~~avec~~

  $`\left\{ \begin{array}{rcl} \gamma_{XX}(\nu) & = & \frac{5}{(\nu^2+4)^2}\\ & & \\ \gamma_{VV}(\nu) & = & \frac{1}{(\nu^2+4)} \end{array} \right.`$

 - ~~Trouver la réponse en fréquence et proposer un schéma de principe pour le filtrage (analogique) : montage série/parallèle pour obtenir l'estimée de S à partir de Y.~~
 - ~~Version discrète du filtre de Wiener causal et anti-causal (fin de la leçon [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf))~~.



####  ~~Séance 2 : Vendredi 13 janvier (10h30-12h30)~~
 - ~~fin exercice de débruitage détaillé sur les [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) p.22~~
 - ~~BE: applications en filtrage inverse~~
   - ~~notebook [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~
   - ~~notebook [N2_deconvolution_image_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N2_deconvolution_image_Wiener_smoothing.ipynb)~~
 - ~~slides [2_Wiener_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/2_Wiener_causal.pdf) : filtre de Wiener **causal** et approche de Bode-Shannon, jusqu'au théorème de Paley-Wiener *slide 10*~~


#### ~~Travail pour le Vendredi 13 janvier (10h30-12h30)~~

- ~~Terminer l'exercice de débruitage détaillé sur les [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) p.22 : Donner l'expression de gain complexe $`G_{\textrm{NC}}(\nu)`$ du filtre optimal et de la densité spectrale de l'erreur $`\gamma_{EE}(\nu)`$ dans les deux cas limites suivants:~~
  1. ~~$\gamma_{VV}(\nu) \rightarrow 0$~~
  2. ~~$\gamma_{VV}(\nu) \rightarrow +\infty$~~
  
  ~~**Déposer vos réponses** sous la forme d'un **document pdf** (export d'un éditeur de texte ou scan pdf de vos notes manuscrites) via ce  [répertoire partagé](https://cloud.univ-grenoble-alpes.fr/s/TPD6T5LKktETZad) (dépot uniquement).~~
- ~~Lire la fin des [slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) sur les exemples de filtrage inverse (pp. 23-29) et exécuter le notebook associé [N1_denoising_chirp_Wiener_smoothing.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks/1_Wiener_non_causal/N1_denoising_chirp_Wiener_smoothing.ipynb)~~


#### ~~Séance 1 : Vendredi 6 janvier (13h30)~~

 - ~~slides [1_Wiener_non_causal.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides/1_Wiener_non_causal.pdf) : introduction + filtre de Wiener non causal + exemples/applications~~
 - ~~TD : exercice slide 22~~

## Bienvenue dans le cours de Filtrage Optimal Sicom !

Vous trouverez dans ce repo gitlab le matériel nécessaire à l'enseignement de *Filtrage optimal* :
  - supports de cours ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/slides))
  - exemples et exercices sous forme de [notebooks python](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks) (fichiers `.ipynb`).
Ces ressources seront actualisées en fonction de l'avancement des séances.


### Comment utiliser les notebooks Python ?

Les exemples et exercices se feront sous python 3.x à travers [scipy](https://www.scipy.org/) et également [scikit-learn](https://scikit-learn.org/).
Ces bibliothèques et modules sont parmi les plus utilisés actuellement pour le calcul numérique et la science des données.

Les *Jupyter Notebooks*  (fichiers `.ipynb`) sont des programmes contenant à la fois des cellules de code (pour nous Python)
et du texte en markdown pour le côté narratif.
Ces notebooks sont souvent utilisés pour explorer et analyser des données. Leur traitement se fait avec une application `jupyter-notebook`, ou `juypyter-lab`, à laquelle on accède par son navigateur web.

Afin de de pouvoir les exécuter vous avez au moins trois possibilités :

1. Téléchargez les notebooks pour les exécuter sur votre machine. Cela requiert d'avoir installé un environnement Python (> 3.3), et les packages Jupyter notebook et scikit-learn. On recommande de les installer via la <a href="https://www.anaconda.com/downloads">distribution Anaconda</a> qui installera directement toutes les dépendances nécessaires.

**Ou**

2. Utiliser un service en ligne `jupyterhub` :

  - je vous recommande celui de l'UGA, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr)  afin que vous puissiez faire tourner les _notebooks_ sur le serveur de calcul de l'UGA tout en sauvegardant vos modifications et vos résultats. Également utile pour lancer un calcul en arrière-plan (connexion avec votre compte Agalan ; nécessite le téléversement _notebooks_+données sur le serveur).
  - Vous pouvez également utiliser un service `jupyterhub` équivalent. Par exemple, celui de google, à savoir [google-colab](https://colab.research.google.com/), qui vous permet d'exécuter/enregistrer vos ordinateurs portables et aussi de _partager l'édition à plusieurs collaborateurs_ (nécessite un compte google et le téléchargement de vos ordinateurs portables+données dans votre _Drive_)

**Ou**

3.  Utiliser le service et les liens _mybinder_ pour les exécuter de manière interactive et à distance (en ligne) : [ ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fds-courses%2Foptimal-filtering-sicom/master?urlpath=lab/tree/notebooks) (ouvrez le lien et attendez quelques secondes que l'environnement se charge).<br>
  **Attention:** Ces _Binder_ sont destinés au codage interactif _ephemère_, ce qui signifie que vos propres modifications/codes/résultats seront perdus lorsque votre session utilisateur s'arrêtera automatiquement (en partique après 10 minutes d'inactivité)


**Note :**  Vous trouverez également parmi les notebooks une introduction à Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/ds-courses/optimal-filtering-sicom/-/tree/master/notebooks%2F0_python_in_a_nutshell)
